import React, { Component } from 'react';
import { Text, View, Dimensions, StyleSheet} from 'react-native';
import MapView from 'react-native-maps';

class PickLocation extends Component {

    state = {
        focusedLocation:{
            latitude: 28.2154,
            longitude: 83.9453,
            latitudeDelta: 0.0122,
            longitudeDelta: Dimensions.get("window").width / 
            Dimensions.get("window").height * 0.0122
        }
    }




  render() {
    return (
      <View style={styles.container}>
        <MapView 
        initialRegion = {
            this.state.focusedLocation
        }
        style = {styles.map}
        />
        <View styles = {styles.button}>
            <Button title = "Locate Me" onPress={() => alert('Pick Locataion')} />
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
    container:{
        width: "100%",
        alignItems: 'center',

    },
    button:{
        margin: 8
    },
    map:{
        width: '100%',
        height: 250

    }
});