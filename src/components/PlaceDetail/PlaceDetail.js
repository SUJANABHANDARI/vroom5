import React, { Component } from 'react'
import { Text, View, Button, Modal, StyleSheet } from 'react-native'
import { selectPlace } from '../../../android/app/src/store/actions';

const placeDetail = props =>{
    let modelContent = null;    
        if (props.selectedPlace){
            modelContent = (
                <View>
                    <Text>{props.selectedPlace.placeName}</Text>
                </View>
            );
        }

    return(
        <Modal>
            <View>
                {modelContent}
                <View>
                    <Button title="Deleted" />
                    <Button title="Close"/>
                        
                </View>
            </View>
        </Modal>

    );
}
const styles = StyleSheet.create({
    modalContainer:{
        margin:22,
    }
});
export default placeDetail